# Price Slider

This project focuses on implementing the following tasks:

- **Task 1 - Bootstrap**: In this task, we use Bootstrap to enhance the user experience on a pricing page. We create a modal that appears when a user clicks on the pricing tables. This modal contains a form asking for the user's Name, Email, and Order Comments. Additionally, we implement a slider that highlights the appropriate pricing plan based on the number of users selected.

- **Task 2 - Vanilla JS**: For this task, we sign up on the website https://forms.maakeetoo.com using vanilla JavaScript.

## Task 1 - Bootstrap

### a. Modal Form

- Integrated a Bootstrap modal that opens when a user clicks on the pricing tables.
- The modal includes a form requesting the following information:
  - [ ] Name
  - [ ] Email
  - [ ] Order Comments

To test this feature, you can click on any of the pricing tables and fill in the form.

### b. Slider for Pricing Highlights

- Implemented a slider that highlights the appropriate pricing plan based on the number of users selected.
- If the number of users falls within a specific range, the corresponding pricing plan will be highlighted.

## Task 2 - Vanilla JS

- In this task, form signs up on https://forms.maakeetoo.com using vanilla JavaScript.


