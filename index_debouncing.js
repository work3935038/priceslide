let freeCard = document.querySelector('.free-card')
let proCard = document.querySelector('.pro-card')
let enterprisecard = document.querySelector('.enterprise-card')
let freeHeader = document.querySelector('.free-header')
let proHeader = document.querySelector('.pro-header')
let enterpriseHeader = document.querySelector('.enterprise-header')  
let chooseFree = document.querySelector('.choose-free')
let choosePro =  document.querySelector('.choose-pro')
let chooseEnterprise = document.querySelector('.choose-enterprise')
let range = document.querySelector('.userrange')
let output = document.querySelector('.outputvalue')
let freeObj = {
    card: freeCard,
    header: freeHeader,
    choose: chooseFree
}

let proObj = {
    card: proCard,
    header: proHeader,
    choose: choosePro
}

let enterpriseObj = {
    card: enterprisecard,
    header: enterpriseHeader,
    choose: chooseEnterprise
}

let currentHighlight;
let oldHighlight;
let currentCard;
let oldcard;
let flag = 0;
let forminput = []

function handleHighlight() {
    console.log("here")
    if (flag) 
    {
        oldHighlight.card.classList.remove('border')
        oldHighlight.card.classList.remove('border-success')
        oldHighlight.header.classList.remove('text-bg-success')
        if (oldcard!=='free')
        oldHighlight.choose.classList.remove('btn-success')
    }
    currentHighlight.card.classList.add('border')
    currentHighlight.card.classList.add('border-success')
    currentHighlight.header.classList.add('text-bg-success')
    if (currentCard !== 'free') {
        currentHighlight.choose.classList.add('btn-success')
    } 
}

const updateRange = debounce(()=>{
    oldHighlight = currentHighlight
    oldcard = currentCard
    if (output.value == 0 && flag == 1)
    {
        oldHighlight.card.classList.remove('border')
        oldHighlight.card.classList.remove('border-success')
        oldHighlight.header.classList.remove('text-bg-success')
        if (oldHighlight.choose.classList.contains('btn-success'))
        {
            oldHighlight.choose.classList.remove('btn-success')
        }
        flag = 0
        return 
    }
    if (output.value <= 20)
    {
        currentHighlight = freeObj
        currentCard = 'free'
    }
    else if(output.value >20 && output.value <=35)
    {
        currentHighlight = proObj
        currentCard = ""
    }
    else
    {
        currentHighlight = enterpriseObj
        currentCard= ""
    }
    handleHighlight()
    flag = 1
}, 250)

range.addEventListener('input', (e)=>{
    output.value = e.target.value
    updateRange()
})

function debounce(cb, delay=500) {
    let timeout

    return ((...args)=>{
        clearTimeout(timeout)
        timeout = setTimeout(()=>{
            cb(...args)
        }, delay)
    })

}

let form = document.querySelector('.order-form')

form.addEventListener('submit', (e)=>{
e.preventDefault()
let formElements = document.getElementsByClassName('form-input')
for (let i = 0; i < formElements.length; i++)
{
    forminput.push(formElements[i].value)
    formElements[i].value=""
}

async function sendFormData(forminput)
{
    const response = await fetch('https://forms.maakeetoo.com/formapi/462', {
        method: 'POST',
        mode: "no-cors",
        headers: {
            "Content-type": 'application/x-www-form-urlencoded'
        },
        body:  `firstname=${forminput[0]}&email=${forminput[1]}&message=${forminput[2]}`

    })
    console.log("here")
}

sendFormData(forminput)

})




/*proCard.classList.add('border')
proCard.classList.add('border-success')
proHeader.classList.add('text-bg-success')
choosePro.classList.add('text-bg-success')*/


